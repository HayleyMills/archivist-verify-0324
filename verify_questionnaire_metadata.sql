-- questionItem labels start with qi_ ;
\qecho '1. qi_label: questionItem labels NOT start with qi_';
COPY (
select i.prefix,
       q.id as question_item_id,
       q.label as question_item_label,
       q.literal as question_item_literal,
       q.instruction_id,
       q.instrument_id,
       q.question_type
from question_items q
left join instruments i on i.id = q.instrument_id
where lower(q.label) not like 'qi_%'
)
TO STDOUT WITH CSV HEADER \g qi_label.csv


-- questionConstruct labels start with qc_ ;
\qecho '2. qc_label: questionConstruct labels NOT start with qc_ ';
COPY (
select i.prefix,
       q.id as cc_questions_id,
       q.instrument_id,
       q.question_id,
       q.question_type,
       q.response_unit_id,
       q.label as cc_questions_label,
       q.parent_id,
       q.parent_type,
       q.position,
       q.branch
from cc_questions q
left join instruments i on i.id = q.instrument_id
where lower(q.label) not like 'qc_%'
)
TO STDOUT WITH CSV HEADER \g qc_label.csv



-- codeList labels start with cs_ ;
\qecho '3. cs_label: codeList labels NOT start with cs_';
COPY (
select i.prefix,
       c.id as code_lists_id,
       c.label as code_lists_label,
       c.instrument_id
from code_lists c
left join instruments i on i.id = c.instrument_id
where lower(c.label) not like 'cs_%'
)
TO STDOUT WITH CSV HEADER \g cs_label.csv



-- questionItems and questionConstruct labels match;
\qecho '4. qi_qc_label: questionItems and questionConstruct labels Not match';
COPY (
select i.prefix,
       question_items.id as question_items_id,
       question_items.label as question_items_label,
       cc_questions.label as cc_questions_label,
       CASE
       when SUBSTRING(question_items.label, 4, length(question_items.label)) != SUBSTRING(cc_questions.label, 4, length(cc_questions.label)) then 0
       else 1
       end as check_label_same
from question_items
left join instruments i on i.id = question_items.instrument_id
left join cc_questions on cc_questions.question_id = question_items.id
where SUBSTRING(question_items.label, 4, length(question_items.label)) != SUBSTRING(cc_questions.label, 4, length(cc_questions.label))
)
TO STDOUT WITH CSV HEADER \g qi_qc_label.csv


-- questions have a response;
\qecho '5. qi_response: question items do NOT have a response';
COPY (
select i.prefix,
       question_items.id as question_items_id,
       question_items.label as question_items_label,
       question_items.instrument_id,
       rds_qs.response_domain_id
from question_items
left join instruments i on i.id = question_items.instrument_id
left join rds_qs on question_items.id = rds_qs.question_id
where rds_qs.response_domain_id is null
)
TO STDOUT WITH CSV HEADER \g qi_response.csv


-- question has a literal;
\qecho '6. qi_literal: question items do NOT have a literal';
COPY (
select i.prefix,
       question_items.id as question_items_id,
       question_items.label as question_items_label,
       question_items.literal as question_items_label,
       question_items.instruction_id,
       question_items.instrument_id,
       question_items.question_type
from question_items
left join instruments i on i.id = question_items.instrument_id
where question_items.literal is null
)
TO STDOUT WITH CSV HEADER \g qi_literal.csv


-- Agency matches the CV;
\qecho '7. agency: Agencies do NOT matches the CV';
COPY (
select agency,
       prefix,
       label,
       study
from instruments
where agency not in ('uk.alspac',
                     'uk.cls.bcs70',
                     'uk.mrcleu-uos.hcs',
                     'uk.cls.mcs',
                     'uk.cls.ncds',
                     'uk.cls.nextsteps',
                     'uk.lha',
                     'uk.mrcleu-uos.sws',
                     'uk.iser.ukhls',
                     'uk.wchads',
                     'uk.mrcleu-uos.heaf',
                     'uk.ons',
                     'uk.genscot'
                     )
)
TO STDOUT WITH CSV HEADER \g agency.csv


-- Study matches the agency;
\qecho '8. study_agency: Studies do NOT matches the agency';
COPY (
SELECT sub.*
FROM (SELECT agency,
             prefix,
             label,
             study,
             CASE
             WHEN upper(study) = 'ALSPAC' then 'ALSPAC'
             WHEN upper(study) = 'BCS' then 'BCS'
             WHEN upper(study) = 'HCS' then 'HCS'
             WHEN upper(study) = 'MCS' then 'MCS'
             WHEN upper(study) = 'NCDS' then 'NCDS'
             WHEN upper(study) = 'NEXT STEPS' then 'Next Steps'
             WHEN upper(study) = 'NSHD' then 'NSHD'
             WHEN upper(study) = 'SWS' then 'SWS'
             WHEN upper(study) = 'US' then 'US'
             WHEN upper(study) = 'WCHADS' then 'WCHADS'
             WHEN upper(study) = 'HEAF' then 'HEAF'
             WHEN upper(study) = 'ONLS' then 'ONLS'
             WHEN upper(study) = 'GENERATION SCOTLAND' then 'Generation Scotland'
             END as correct_study,
             CASE
             WHEN upper(study) = 'ALSPAC' then 'uk.alspac'
             WHEN upper(study) = 'BCS' then 'uk.cls.bcs70'
             WHEN upper(study) = 'HCS' then 'uk.mrcleu-uos.hcs'
             WHEN upper(study) = 'MCS' then 'uk.cls.mcs'
             WHEN upper(study) = 'NCDS' then 'uk.cls.ncds'
             WHEN upper(study) = 'NEXT STEPS' then 'uk.cls.nextsteps'
             WHEN upper(study) = 'NSHD' then 'uk.lha'
             WHEN upper(study) = 'SWS' then 'uk.mrcleu-uos.sws'
             WHEN upper(study) = 'US' then 'uk.iser.ukhls'
             WHEN upper(study) = 'WCHADS' then 'uk.wchads'
             WHEN upper(study) = 'HEAF' then 'uk.mrcleu-uos.heaf'
             WHEN upper(study) = 'ONLS' then 'uk.ons'
             WHEN upper(study) = 'GENERATION SCOTLAND' then 'uk.genscot'
             END as correct_agency
      FROM instruments
      ) sub
WHERE sub.study != sub.correct_study
OR    sub.agency != sub.correct_agency
)
TO STDOUT WITH CSV HEADER \g study_agency.csv


-- output all labels from questions/loops/conditions ;
\qecho '9. all_labels: output all labels from questions/loops/conditions';
COPY (
select i.prefix as prefix,
       qi.label as label
from question_items qi
left join instruments i on i.id = qi.instrument_id
UNION
select i.prefix as prefix,
       qg.label as label
from question_grids qg
left join instruments i on i.id = qg.instrument_id
UNION
select i.prefix as prefix,
       qc.label as label
from cc_questions qc
left join instruments i on i.id = qc.instrument_id
UNION
select i.prefix as prefix,
       l.label as label
from cc_loops l
left join instruments i on i.id = l.instrument_id
UNION
select i.prefix as prefix,
       c.label as label
from cc_conditions c
left join instruments i on i.id = c.instrument_id
order by prefix, label
)
TO STDOUT WITH CSV HEADER \g all_labels.csv


-- condition labels match the first question in the logic ;
\qecho '10. qi_label: condition labels do NOT match the first question in the logic';
COPY (
select i.prefix,
       c.label,
       c.logic,
       replace(substr(c.label, 4), ' ', '') as condition_label_name,
       replace(substr(split_part(split_part(c.logic, '||', 1), '==', 1), 4), ' ', '') as logic_question_name
from cc_conditions c
left join instruments i on i.id = c.instrument_id
where replace(substr(c.label, 4), ' ', '') != replace(substr(split_part(split_part(c.logic, '||', 1), '==', 1), 4), ' ', '')
)
TO STDOUT WITH CSV HEADER \g condition_label_logic.csv
