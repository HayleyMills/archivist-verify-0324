from pathlib import Path
import os
import pandas as pd


prefix = os.environ.get('PREFIX')

Path("prefix_dir").mkdir(exist_ok=True)

if prefix is not None:

    prefix_dir = Path("prefix_dir") / prefix
    prefix_dir.mkdir(exist_ok=True)

    # subset
    for csv in Path(".").glob("*.csv"):
        if csv != 'no_missing_labels.csv':
            df = pd.read_csv(csv)
            if not df.empty:
                df_sub = df[df["prefix"] == prefix]
                if not df_sub.empty:
                    df_sub.to_csv(prefix_dir / csv.name, index=False)
